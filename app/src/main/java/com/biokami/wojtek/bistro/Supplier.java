package com.biokami.wojtek.bistro;

/**
 * Created by nirav on 21/02/16.
 */
public class Supplier {

    private static int counter = 0;

    public final int objectId;

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Supplier(String name) {
        this.name = name;
        this.objectId = counter++;
    }
}

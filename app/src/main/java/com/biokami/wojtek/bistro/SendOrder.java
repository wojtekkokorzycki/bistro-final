package com.biokami.wojtek.bistro;

import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class SendOrder extends AppCompatActivity {

    EditText to;
    EditText cc;
    EditText subject;
    EditText msgBody;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_order);
        to = (EditText) findViewById(R.id.to);
        cc = (EditText) findViewById(R.id.cc);
        subject = (EditText) findViewById(R.id.subject);
        msgBody = (EditText) findViewById(R.id.msgBody);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.send_menu, menu);
        MenuItem sendItem = menu.findItem(R.id.send);
        MenuItem cancelItem = menu.findItem(R.id.cancel);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.send) {

            Toast.makeText(this, "" + to.getText() + " " + cc.getText() + " " + subject.getText() + " " + msgBody.getText(),
                    Toast.LENGTH_LONG).show();
            return true;
        }

        if (id == R.id.cancel) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
